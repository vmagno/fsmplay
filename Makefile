PROGRAM=fsmPlay

CXX=g++

all: $(PROGRAM)

$(PROGRAM): main.cpp FiniteStateMachine.h FiniteStateMachine.cpp
	$(CXX) -Wall -Wextra -pedantic  main.cpp FiniteStateMachine.cpp -o $(PROGRAM)

run: $(PROGRAM)
	./$(PROGRAM)

clean:
	rm -f $(PROGRAM)

