#include <iostream>

#include "FiniteStateMachine.h"


int main(void)
{

    std::cout << "Reading state machine from file (trying to)\n";
    const auto fsm = FiniteStateMachine::readFromFile("fsm.txt");

//     FiniteStateMachine fsm(0, {1}, {0, 1},
//             /*  E0  E1   */ 
//     /* Q0 */  {{1,  0},
//     /* Q1 */   {0,  0}
//             });

    bool done = false;
    while (!done)
    {
        std::cout << "Please enter a string: \n";
        
        std::string inString;
        std::cin >> inString;


        std::vector<int> inputInts;
        for (const auto c : inString)
        {
            if (c >= '0' && c <= '9')
            {
                inputInts.push_back(c - '0');
            }
            else
            {
                std::cout << "INVALID INPUTTTTTTTT\n";
                done = true;
                break;
            }
        }

        std::cout << "Your input string: ";
        for (const auto i : inputInts) std::cout << i << ", ";
        std::cout << std::endl;

        if (fsm.accepts(inputInts))
            std::cout << "Accepted!\n";
        else
            std::cout << "Rejected!\n";

    }


    return 0;
}
