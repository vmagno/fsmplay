#include "FiniteStateMachine.h"

#include <fstream>
#include <sstream>
#include <iostream>

FiniteStateMachine FiniteStateMachine::readFromFile(const std::string& filename)
{
    std::fstream inFile(filename);
    std::string line;


    std::vector<std::string> states;
    std::vector<std::string> alphabet;
    std::unordered_set<int> acceptStates;
    std::vector<std::vector<int>> transitions;

    int startState = 0;
    int numStates = 0;
    int numSymbols = 0;

    while (std::getline(inFile, line))
    {
        if (line.empty())
            continue;

        std::istringstream lineStream(line);
        std::string header;
        lineStream >> header;
        
        if (header.empty() || header[0] == '#')
        {
        }
        if (header.compare("STATES:") == 0)
        {
            std::string stateName;
            while (lineStream >> stateName)
                states.push_back(stateName);
        }
        else if (header.compare("ALPHABET:") == 0)
        {
            std::string symbol;
            while (lineStream >> symbol)
                alphabet.push_back(symbol);

            std::cout << "Alphabet: \n";
            for (const auto& s : alphabet
                )
            {
                std::cout << s << std::endl;
            }
        }
        else if (header.compare("START:") == 0)
        {
            lineStream >> startState;
            std::cout << "Startstate: " << startState << std::endl;
        }
        else if (header.compare("ACCEPT:") == 0)
        {
            int state;
            while (lineStream >> state)
            {
                acceptStates.insert(state);
            }

            for (const auto& s : acceptStates)
            {
                std::cout << "accept : " << s << std::endl;
            }
        }
        else if (header.compare("NUM_STATES:") == 0)
        {
            lineStream >> numStates;
        }
        else if (header.compare("NUM_SYMBOLS:") == 0)
        {
            lineStream >> numSymbols;
        }
        else if (header.compare("TRANSITIONS:") == 0)
        {
            transitions.resize(numStates);
            for (int i = 0; i < numStates; i++) {
                for (int j = 0; j < numSymbols; j++)
                {
                    int next;
                    lineStream >> next;
                    transitions[i].push_back(next);
                }
            }
        }
    }

    return FiniteStateMachine(startState, acceptStates, transitions);
}

bool FiniteStateMachine::accepts(const std::vector<int>& inputString) const
{
    auto currentState = startState_;

    for (const auto symbol : inputString)
    {
        currentState = stateTransitions_[currentState].at(symbol);
    }

    if (acceptStates_.count(currentState) > 0)
        return true;
    else 
        return false;
}
