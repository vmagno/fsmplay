#include <string>
#include <unordered_set>
#include <vector>

class FiniteStateMachine
{
public:
    FiniteStateMachine(
            //const std::vector<int> states,
            //const std::vector<int>& alphabet,
            const int startState,
            const std::unordered_set<int>& acceptStates,
            const std::vector<std::vector<int>>& transitions)
        :
            //states_(states),
            //alphabet_(alphabet),
            startState_(startState),
            acceptStates_(acceptStates),
            stateTransitions_(transitions)
    {
    }


    static FiniteStateMachine readFromFile(const std::string& filename);

    bool accepts(const std::vector<int>& inputString) const;

private:
    //std::vector<int> states_;
    //std::vector<int> alphabet_;
    int startState_;
    std::unordered_set<int> acceptStates_;
    std::vector<std::vector<int>> stateTransitions_;
};

